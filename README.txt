--[[

hi! thanks for using my recreation of the isle weapon system!

the development of this system started around 2022/04/14 (YY/MM/DD)

--{INSTRUCTIONS}--

[INSTALLATION]
-put all the models in their respective place and ungroup (CTRL + U)
-upload animations from the animations dummy to roblox and replace the ids with yours
-set avatar type to R15 (Game Settings > Avatar)

[MAKING A NEW GUN]
-clone "ExampleGun" tool (CTRL + D)
-configure the stats
-if you want to have a custom model make SURE that in the handle theres: 
Aim (Sound), Empty (Sound), Equip (Sound), FireAttachment (Attachment), Fire (Sound) (inside FireAttachment) and RayAttachment (Attachment)
-to add your own fire particles make sure that they are inside FireAttachment and have a NumberValue named "Emit" inside them (this controls how many particles it emits each shot)
-if there isnt the following items: Animations, Values, Inserter, Fire, ServerFire or SettingsModule inside the tool then clone them from the ExampleGun

[REPORTING BUGS]
-before you do ANYTHING make sure the version you have is mine (timotej1181) and from the gitlab. im not gonna fix bugs that someone else introduced. please be wary.
-make sure you didn't edit the code, i wont fix your own bugs for you.
-dm me on discord @timotej1181.
-make sure to send the output log. (if there is any errors)

[MIGRATING TO A NEW VERSION]
-bassicaly the same as installation except with one more step
-check the update log for which new settings may have been added to tools and copy them
-paste in each of the SettingsModules in the right tool type


--{CREDITS}--

-timotej1181 - 99% of the scripting

-East98 - Combine_ArrayTables function
https://devforum.roblox.com/t/combining-multiple-tables-together-only-adds-the-first-table/1153876/3

-by jelliedbanana - spread logic
https://devforum.roblox.com/t/help-with-spread-script/2280072/2

-MooMooManager and orange451 - for their visualization of wall banging
https://devforum.roblox.com/t/bullet-wallbanging-is-not-functioning-properly/1031719/8
https://devforum.roblox.com/t/scripting-a-bullet-wallbang-with-raycasting/1979686/2

-boatbomber - BoatTween module
https://devforum.roblox.com/t/boattween-module/540277

--{UPDATE LOG}--
[V1.0]
- release

[v1.0.1]
- replaced all wait() with task.wait()
- removed tool from function return table
- added the isle_msg,effect,debugeffect to _G table (client only)
- added these options to SettingsModule: 
BarrelCanBeBlocked: determines if the barrel can be blocked by parts
BarrelBlockedText: text that displays if the barrel is being blocked
BarrelBlockedTextColor: the color of BarrelBlockedText 
TargetBehindWallText: displays if the target is behind a wall (before aim loop begins)
TargetBehindWallTextColor: color of TargetBehindWallText

[v1.0.2]
- "alert_gui" now automaticly puts it self inside the PlayerGui
- fixed tool_client only checking if the barrel is blocked only on PC, now it works on both PC and Mobile/Tablet
- hopefully fixed inaccurate aim times
- HitText no longer requires a space after the text
- humanoid is now used for animations incase the Animator object wasnt found
- knockback is now applied using AssemblyLinearVelocity instead of the "BodyPosition" BodyMover
- removed "server_char"
- removed "Speed" as the new method of applying knockback only needs 1 value
- removed check to see if we hit the same humanoid twice (text only)
- added "ExampleAmmoBox" Tool
- added "CurrentAmmo" NumberValue to Values folder
- added "Counter" NumberValue to Values folder
- added WeaponCooldown attribute to humanoid, wich means you cannot use other weapons until the current weapon's RecoverTime is finished
- added pierce_func: fires when the bullet pierces a wall
- added new options to SettingsModule: 
JumpPower: same as walkspeed but with jumpower
NPCinfiniteammo: boolean determining if the weapon should consume ammo if fired via ServerFire bindable event
TracerEnabled: boolean determining if the weapon should have bullet tracers
TracerSettings: table containing settings used by the tracer function
BulletHoleEnabled: boolean used for determining if bullet holes should be enabled or not
BulletHoleMap: folder containing information on particle and sound should be used on wich material
TargetHitText: text that displays for the target once succesfully hit
LineOfSightRequired: boolean determining if you need line of sight to fire (only checked on client)
RespectWeaponCoolDown: boolean determining if the tool should fire regardless of the "WeaponCooldown" attribute

[v1.0.2 hotfix 1] (24/10/2023)(DD/MM/YY)
- hopefully fixed WarningText setting breaking if shooting an NPC (thanks @TheHatGuy-zf7uh)

[v1.0.2 hotfix 2] (4/12/2023)(DD/MM/YY)
- hopefully fixed ExampleAmmoBox getting stuck in a infinite loop (thanks ALVES)

[v1.0.2 hotfix 3] (22/12/2023)(DD/MM/YY)
- hopefully fixed humanoids being able get hit more than 1 time per shot if piercing is high enough (thanks awesomeguy)

[v1.0.2 hotfix 4] (23/12/2023)(DD/MM/YY)
- hopefully fixed Force flinging insane amount of studs even when set to low numbers (thanks awesomeguy)
- added ["ForcePerBullet"] option to ExampleTool (determines if force will be applied per bullet or spread out between bulelts) ( Force = Force/BulletsPerShot )

[v1.0.3] (2/1/2024)(DD/MM/YY)
- fixed NPCinfiniteammo option not working (thanks Dragon)
- centered the text gui to the center of the screen (thanks Dragon)
- hopefully fixed aimtime being longer than its supposted to (thanks everyone who reported this bug)
- added a Simple NPC template (NPC script disabled by default)
- added new options to SettingsModule:
ForcePerBurst: option to ExampleTool (determines if force will be split across the burst) ( Force = Force/Burst )
MinRange: minimum range of the weapon
TooCloseText: displays if in the minimum range of the weapon
TargetMovedTooCloseText: same as TooCloseText except it displays while the aim loop is active
SitOnHit: makes the target humanoid sit upon being hit (Yes that means Stun no longer makes you sit)

[v1.0.3 hotfix 1] (8/1/2024)(DD/MM/YY)
- hopefully fixed ExampleAmmoBox breaking if the player has a tool in the backpack without a "SettingsModule" (thanks ErnestoCaraGordaHH)

[v1.0.3 hotfix 2] (17/1/2024)(DD/MM/YY)
- ExampleTool and ExampleAmmoBox tooltip now updates everytime the Ammo value inside both is changed
- hopefully fixed ExampleAmmoBox attempting to refill ammo even if it has 0 left (thanks ErnestoCaraGordaHH)
- fixed ExampleTool consuming ammo if the barrel was blocked while trying to shoot (thanks ErnestoCaraGordaHH)

[v1.0.3 hotfix 3] (10/3/2024)(DD/MM/YY)
- fixed knockback (thanks to ledsohck1166)

[v1.0.4] (30/6/2024)(DD/MM/YY)
- fixed ExampleAmmoBox stopping refill while sitting in a moving vehicle (or just sitting in general)
- corrected misspelling of the word "default" in the code (thanks Dragon)
- added shoot_func
- added more ways to define what shows up in the isle text (such as the shooter, target, etc... (check SettingsModule in ExampleTool))

[1.0.4 hotfix 1] (9/8/2024)(DD/MM/YY)
- fixed TooFarAwayText not working at all (thanks tennis)
- fixed TooFarAwayText not appearing if above gun range * 1.5 (was a bad idea in the first place)

[1.0.4 hotfix 2] (20/9/2024)(DD/MM/YY)
- renamed ExampleTool to ExampleGun (it just fits better)
- added ["CloneFireSoundEveryShot"] to ExampleGun SettingsModule

just to help with the migration process of every update ill post all the settings which have been added at the bottom of the update's changelog
so you can just copy and paste it into the SettingsModule of each tool you made instead of having to reconfigure everything again.
MAKE. SURE. TO. PASTE. IN. THE. CORRECT. TOOL. TYPE.
for example: "Gun" would refer to all the guns which can shoot and all that. "AmmoBox" would refer to the ammoboxes for said guns.

"Gun" ---VVV
["CloneFireSoundEveryShot"] = false, --prevents sound from cutting off when shot, some may want this effect

[1.0.4 hotfix 3] (15/11/2024)(DD/MM/YY)
- fixed torso joint using default C0 instead of getting the character joint C0

]]--

script:Destroy() --don't litter